import datetime
import configparser
import re
import json
import requests
import os
import math
from flask import Flask, render_template, redirect, jsonify, request, abort
from flask_restful import Resource, Api
from flask_cors import CORS
import sqlite3


app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/api": {"origins": "*"}})

configParser = configparser.ConfigParser()
configFilePath = r'config.properties'
configParser.read(configFilePath)
databasePath = configParser.get('config', 'databasePath')


def setUpDatabase():
    conn = sqlite3.connect(databasePath)
    conn.execute(
        'CREATE TABLE IF NOT EXISTS LOG (id integer PRIMARY KEY, operation TEXT, processed_timestamp integer)')
    conn.close()


setUpDatabase()


def getReport(startDate, endDate):
    start_date = datetime.datetime.strptime(startDate, "%Y%m%d%H%M%S")
    end_date = datetime.datetime.strptime(endDate, "%Y%m%d%H%M%S")
    conn = sqlite3.connect(databasePath)
    cur = conn.cursor()
    cur.execute(
        "SELECT * FROM log WHERE processed_timestamp >= ? AND processed_timestamp <= ? ", (startDate, endDate))
    logs = cur.fetchall()
    conn.close()
    return logs


def getAllReport():
    data = json.load(open('static/swagger.json'))
    return jsonify(results=data)


def log(operation):
    processed_timestamp = datetime.datetime.today().strftime("%Y%m%d%H%M%S")
    conn = sqlite3.connect(databasePath)
    conn.execute(
        "INSERT INTO LOG (operation, processed_timestamp) VALUES (?,?)", (operation, processed_timestamp))
    conn.commit()
    conn.close()


@app.route("/")
def welcome():
    return render_template("index.html")


@app.route("/report")
def report():
    return render_template("report.html")


class ReportGenerationEngine(Resource):
    def get(self):
        args = request.args
        startDate = args['from']
        endDate = args['to']
        result = getReport(startDate, endDate)
        return jsonify(results=result)


class GetAPIDocs(Resource):
    def get(self):
        with open('static/swagger.json') as f:
            data = json.load(f)
            return jsonify(**data)


class AddEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = dataDict['x'] + dataDict['y']
        result = roundResult(result)
        log(str(dataDict['x']) + " + " +
            str(dataDict['y']) + " = " + str(result))
        return {'result': result}


class SubtractEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = dataDict['x'] - dataDict['y']
        result = roundResult(result)
        log(str(dataDict['x']) + " - " +
            str(dataDict['y']) + " = " + str(result))
        return {'result': result}


class MultiplyEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = dataDict['x'] * dataDict['y']
        result = roundResult(result)
        log(str(dataDict['x']) + " * " +
            str(dataDict['y']) + " = " + str(result))
        return {'result': result}


class DivideEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        if(dataDict['y'] == 0):
            abort(400, "Divider cannot be zero")
        result = dataDict['x'] / dataDict['y']
        result = roundResult(result)
        log(str(dataDict['x']) + " / " +
            str(dataDict['y']) + " = " + str(result))
        return {'result': result}


class SqaureRootEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = dataDict['x']**(1/2)
        result = roundResult(result)
        log(str(dataDict['x']) + " ^(1/2)" + " = " + str(result))
        return {'result': result}


class CubeRootEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = dataDict['x']**(1/3)
        result = roundResult(result)
        log(str(dataDict['x']) + " ^(1/3)" + " = " + str(result))
        return {'result': result}


class PowerEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = dataDict['x']**(dataDict['y'])
        result = roundResult(result)
        log(str(dataDict['x']) + " ^" +
            str(dataDict['y']) + " = " + str(result))
        return {'result': result}


class FactorialEngine(Resource):
    def post(self):
        if not request.json:
            abort(400)
        dataDict = json.loads(request.data)
        result = math.factorial(dataDict['x'])
        result = roundResult(result)
        log(str(dataDict['x']) + "!" + " = " + str(result))
        return {'result': result}


def roundResult(result):
    return float(format(result, '.4g'))


api.add_resource(AddEngine, '/operation/add')
api.add_resource(SubtractEngine, '/operation/subtract')
api.add_resource(MultiplyEngine, '/operation/multiply')
api.add_resource(DivideEngine, '/operation/divide')
api.add_resource(SqaureRootEngine, '/operation/squareroot')
api.add_resource(CubeRootEngine, '/operation/cuberoot')
api.add_resource(PowerEngine, '/operation/power')
api.add_resource(FactorialEngine, '/operation/factorial')
api.add_resource(ReportGenerationEngine, '/generatereport')
api.add_resource(GetAPIDocs, '/api')


test_data = {
    'x': 25,
    'y': 2
}

with app.test_client() as c:

    rv = c.post('/operation/add', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 27.0000
    rv = c.post('/operation/subtract', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 23.0000
    rv = c.post('/operation/multiply', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 50.0000
    rv = c.post('/operation/divide', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 12.5000
    rv = c.post('/operation/squareroot', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 5.0000
    rv = c.post('/operation/cuberoot', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 2.9240
    rv = c.post('/operation/power', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert json_data['result'] == 625.0000
    rv = c.post('/operation/factorial', data=json.dumps(test_data),
                headers={'Content-Type': 'application/json'})
    json_data = json.loads(rv.data)
    assert roundResult(json_data['result']) == 1.551e+25

if __name__ == '__main__':
    debugMode = False
    if(os.environ['ENV'] == 'dev'):
        debugMode = True
    app.run(debug=debugMode, host='0.0.0.0')
