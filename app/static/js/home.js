function toggleWarning(show) {
  var warn = document.getElementById("warn");
  if (show) {
    warn.style.display = "block";
    document.getElementById("result").innerHTML = ""
  } else {
    warn.style.display = "none";
  }
}

function onOperationChange() {
  console.log("Select changed")
  var select = document.getElementById("operation");
  var selection = select.options[select.selectedIndex].value;
  var ydiv = document.getElementById("ydiv");

  if (selection === 'squareroot' || selection === 'cuberoot' || selection === 'factorial') {
    ydiv.style.display = "none";
  } else {
    ydiv.style.display = "block";
  }
}

function validate() {
  var select = document.getElementById("operation");
  var selection = select.options[select.selectedIndex].value;

  if (selection == 0) {
    document.getElementById("error").innerHTML = "Please select the operation!"
    toggleWarning(true)
  } else {
    toggleWarning(false)

    var data = {
      'x': parseFloat(document.getElementById("x").value),
      'y': parseFloat(document.getElementById("y").value)
    }

    submit(selection, data)
  }
}
function submit(operation, data) {
  axios({
    method: `post`,
    url: "operation/" + operation,
    data: data
  }).then(function (response) {
    document.getElementById("result").innerHTML = response.data.result
  }).catch(function (error) {
    console.log(error);
    document.getElementById("error").innerHTML = error.response.data.message
    toggleWarning(true)
  });
}