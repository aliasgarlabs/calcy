var startDate, endDate;

$(function () {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        startDate = start.format('YYYYMMDDHHmmss')
        endDate = end.format('YYYYMMDDHHmmss')
        startDate = moment(startDate, 'YYYYMMDDHHmmss').utc().format('YYYYMMDDHHmmss')
        endDate = moment(endDate, 'YYYYMMDDHHmmss').utc().format('YYYYMMDDHHmmss')
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});

function submit() {
    axios({
        method: `get`,
        url: "generatereport?from=" + startDate + "&to=" + endDate,
    }).then(function (response) {
        console.log(response.data.results)

        var x = "";
        var i = 0;
        response.data.results.forEach(function (arrayItem) {
            i++;
            x += '<tr><th scope="row">' + i + '</th><td>' + arrayItem[1] + '</td> <td>' + arrayItem[2] + '</td></tr>';
            console.log(x);
        });

        if (response.data.results.length == 0) {
            x = 'No operations found';
        }

        document.getElementById('table_body').innerHTML = x;

    }).catch(function (error) {
        console.log(error);
    });
}
