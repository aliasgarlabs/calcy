# CollPoll Web Application

### Getting Started

Pre-requisites:
- Docker 
- Docker-compose

Set-up:
- Navigate to docker/development folder
- Run `docker-compose up`

#### Calcy Web App
Try `http://localhost:5000` on your browser.

#### Calcy API Documentation
Try `http://localhost:3000` on your browser.

This project supports development with docker which means any dependencies apart from docker and docker-compose are not required to be installed for development. Any changes to source files will automatically detected by the server and it will update itself. 